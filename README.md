# StudIP GitLab Sync

Synchroniziere StudIP Veranstaltungen mit GitLab Repos.

## Aufsetzen

1. Erstelle ein GitLab Repo für deine StudIP Veranstaltung.
2. Klone dein Repo und gehe rein.
3. Führe in dem Repo den folgenden Befehl aus: `curl -o "./update.sh" "https://gitlab.gwdg.de/j.vondoemming/studip-gitlab-sync/-/raw/main/update.sh?ref_type=heads&inline=false"`
4. Füge eine `config.json` nach unterem Schema hinzu.
5. Führe `./update.sh` aus.
6. Erstelle eine Datei `template/loesungtemplate.tex`. Diese wird automatisch für jedes Übungsblatt mitkopiert und die Strings `((BLATTNUMMER))` und `((ABGABEDATUM))` dadrin entsprechend ersetzt.
6. Optional: Setze [`glab`](https://gitlab.com/gitlab-org/cli#installation) auf um automatisch Issues zu erstellen.
7. Commite und Pushe!

Du kannst nun jederzeit `./update.sh` ausführen um die Daten von StudIP mit deinem Repo zu synchronizieren.

## config.json Schema

```json
{
	"name": "<Name der StudIP Veranstaltung>",
	"link": "<Link den man bekommt, wenn man in der Veranstaltungs Übersicht auf 'Link zu dieser Veranstaltung kopieren' klickt.>",
	"hat_uebungsblaetter": true | false <Ob die Veranstaltung Übungsblätter hat>,
	"aufgabenblatt_ordner": "<Ordnername auf StudIP in dem die Übungsblätter sind>",
	"uebungen_ordner": "<Ordnername im Repo wo die Übungsblätter rein sollen. Muss echt verschieden sein von aufgabenblatt_ordner. Empfohlen: uebung>",
	"uebungen": {
        <Nach dem folgendem Muster aus dem Beispiel. aufgabenblatt ist der Name der Datei in aufgabenblatt_ordner>
		"01": { "aufgabenblatt": "Uebung_01.pdf", "abgabe": "03.11.2023, 10 Uhr" },
		"02": { "aufgabenblatt": "Uebung_02.pdf", "abgabe": "10.11.2023, 10 Uhr" },
		"03": { "aufgabenblatt": "Uebung_03.pdf", "abgabe": "17.11.2023, 10 Uhr" },
		"04": { "aufgabenblatt": "Uebung_04.pdf", "abgabe": "10.11.2023, 10 Uhr" },
		"05": { "aufgabenblatt": "Uebung_05.pdf", "abgabe": "24.11.2023, 10 Uhr" },
		"06": { "aufgabenblatt": "Uebung_06.pdf", "abgabe": "01.12.2023, 10 Uhr" },
		"07": { "aufgabenblatt": "Uebung_07.pdf", "abgabe": "08.12.2023, 10 Uhr" },
		"08": { "aufgabenblatt": "Uebung_08.pdf", "abgabe": "15.12.2023, 10 Uhr" },
		"09": { "aufgabenblatt": "Uebung_09.pdf", "abgabe": "22.12.2023, 10 Uhr" },
		"10": { "aufgabenblatt": "Uebung_10.pdf", "abgabe": "29.12.2023, 10 Uhr" },
		"11": { "aufgabenblatt": "Uebung_11.pdf", "abgabe": "05.01.2024, 10 Uhr" },
		"12": { "aufgabenblatt": "Uebung_12.pdf", "abgabe": "12.01.2024, 10 Uhr" }
	},
	"uebungen_extra": {
        <Nach dem folgendem Muster aus dem Beispiel. Beschreibt was mit den extra Dateien in dem aufgabenblatt_ordner gemacht werden soll>
		"Uebung_03.ipynb": "uebung/03/",
		"Uebung_04.ipynb": "uebung/04/",
		"Uebung_05.ipynb": "uebung/05/",
		"Uebung_06.ipynb": "uebung/06/",
		"Uebung_07.ipynb": "uebung/07/",
		"Uebung_08.ipynb": "uebung/08/",
		"Uebung_09.ipynb": "uebung/09/",
		"Uebung_10.ipynb": "uebung/10/",
		"Uebung_11.ipynb": "uebung/11/",
		"Uebung_12.ipynb": "uebung/12/"
	},
	"issue": {
		"title": "<Veranstaltungsname> Übungsblatt ((BLATTNUMMER)) bis ((ABGABEDATUM))",
		"description": [
			"Bearbeite <Veranstaltungsname> Übungsblatt ((BLATTNUMMER)) bis ((ABGABEDATUM)).",
			"",
			"/due ((ABGABEDATUMDDMMYYYY))"
		],
		"assignee": "j.vondoemming",
		"label": "Uebungsblatt"
	},
	"template_links": [<Liste an Dateien die aus dem template Ordner in jeden Übungsordner verlinkt werden sollen.>]
}
```

### Beispiele

```json
{
	"name": "Vorlesung: Experimentalphysik I (WiSe 2023/24)",
	"link": "https://studip.uni-goettingen.de/dispatch.php/course/details?sem_id=b88ee5aeccc8bca2fe0ac098c4dfcea6&again=yes",
	"hat_uebungsblaetter": true,
	"aufgabenblatt_ordner": "Uebungsblaetter",
	"uebungen_ordner": "uebung",
	"uebungen": {
		"01": { "aufgabenblatt": "Uebung_01.pdf", "abgabe": "03.11.2023, 10 Uhr" },
		"02": { "aufgabenblatt": "Uebung_02.pdf", "abgabe": "10.11.2023, 10 Uhr" },
		"03": { "aufgabenblatt": "Uebung_03.pdf", "abgabe": "17.11.2023, 10 Uhr" },
		"04": { "aufgabenblatt": "Uebung_04.pdf", "abgabe": "10.11.2023, 10 Uhr" },
		"05": { "aufgabenblatt": "Uebung_05.pdf", "abgabe": "24.11.2023, 10 Uhr" },
		"06": { "aufgabenblatt": "Uebung_06.pdf", "abgabe": "01.12.2023, 10 Uhr" },
		"07": { "aufgabenblatt": "Uebung_07.pdf", "abgabe": "08.12.2023, 10 Uhr" },
		"08": { "aufgabenblatt": "Uebung_08.pdf", "abgabe": "15.12.2023, 10 Uhr" },
		"09": { "aufgabenblatt": "Uebung_09.pdf", "abgabe": "22.12.2023, 10 Uhr" },
		"10": { "aufgabenblatt": "Uebung_10.pdf", "abgabe": "29.12.2023, 10 Uhr" },
		"11": { "aufgabenblatt": "Uebung_11.pdf", "abgabe": "05.01.2024, 10 Uhr" },
		"12": { "aufgabenblatt": "Uebung_12.pdf", "abgabe": "12.01.2024, 10 Uhr" }
	},
	"uebungen_extra": {
		"Uebung_03.ipynb": "uebung/03/",
		"Uebung_04.ipynb": "uebung/04/",
		"Uebung_05.ipynb": "uebung/05/",
		"Uebung_06.ipynb": "uebung/06/",
		"Uebung_07.ipynb": "uebung/07/",
		"Uebung_08.ipynb": "uebung/08/",
		"Uebung_09.ipynb": "uebung/09/",
		"Uebung_10.ipynb": "uebung/10/",
		"Uebung_11.ipynb": "uebung/11/",
		"Uebung_12.ipynb": "uebung/12/"
	},
	"issue": {
		"title": "ExPhy 1 Übungsblatt ((BLATTNUMMER)) bis ((ABGABEDATUM))",
		"description": [
			"Bearbeite ExPhy 1 Übungsblatt ((BLATTNUMMER)) bis ((ABGABEDATUM)).",
			"",
			"/due ((ABGABEDATUMDDMMYYYY))"
		],
		"assignee": "j.vondoemming",
		"label": "Uebungsblatt"
	},
	"template_links": []
}
```

```json
{
	"name": "Vorlesung: Diskrete Stochastik für Studierende der Informatik (WiSe 2023/24)",
	"link": "https://studip.uni-goettingen.de/dispatch.php/course/details?sem_id=52973f697044668b020c46560e64799a&again=yes",
	"hat_uebungsblaetter": true,
	"aufgabenblatt_ordner": "Übungsblätter",
	"uebungen_ordner": "uebung",
	"uebungen": {
		"00": { "aufgabenblatt": "blatt00.pdf", "abgabe": "N/A" },
		"01": { "aufgabenblatt": "blatt01.pdf", "abgabe": "03.11.2023, 14 Uhr" },
		"02": { "aufgabenblatt": "blatt02.pdf", "abgabe": "10.11.2023, 14 Uhr" },
		"03": { "aufgabenblatt": "blatt03.pdf", "abgabe": "17.11.2023, 14 Uhr" },
		"04": { "aufgabenblatt": "blatt04.pdf", "abgabe": "24.11.2023, 14 Uhr" },
		"05": { "aufgabenblatt": "blatt05.pdf", "abgabe": "01.12.2023, 14 Uhr" },
		"06": { "aufgabenblatt": "blatt06.pdf", "abgabe": "08.12.2023, 14 Uhr" },
		"07": { "aufgabenblatt": "blatt07.pdf", "abgabe": "15.12.2023, 14 Uhr" },
		"08": { "aufgabenblatt": "blatt08.pdf", "abgabe": "22.12.2023, 14 Uhr" },
		"09": { "aufgabenblatt": "blatt09.pdf", "abgabe": "29.12.2023, 14 Uhr" },
		"10": { "aufgabenblatt": "blatt10.pdf", "abgabe": "05.01.2024, 14 Uhr" },
		"11": { "aufgabenblatt": "blatt11.pdf", "abgabe": "12.01.2024, 14 Uhr" },
		"12": { "aufgabenblatt": "blatt12.pdf", "abgabe": "19.01.2024, 14 Uhr" }
	},
	"issue": {
		"title": "Disto Übungsblatt ((BLATTNUMMER)) bis ((ABGABEDATUM))",
		"description": [
			"Bearbeite Disto Übungsblatt ((BLATTNUMMER)) bis ((ABGABEDATUM)).",
			"",
			"/due ((ABGABEDATUMDDMMYYYY))"
		],
		"assignee": "j.vondoemming",
		"label": "Uebungsblatt"
	},
	"template_links": ["Makefile", "latexindent.yaml"]
}
```

```json
{
	"name": "Vorlesung mit Übung: Technische Informatik (WiSe 2023/24)",
	"link": "https://studip.uni-goettingen.de/dispatch.php/course/details?sem_id=39b7856a4a70a9ef682d2f302b1e7bd5&again=yes",
	"hat_uebungsblaetter": false,
	"aufgabenblatt_ordner": "",
	"uebungen_ordner": "",
	"uebungen": {
	},
	"issue": {
		"title": "Technische-Informatik Übungsblatt ((BLATTNUMMER)) bis ((ABGABEDATUM))",
		"description": [
			"Bearbeite Technische-Informatik Übungsblatt ((BLATTNUMMER)) bis ((ABGABEDATUM)).",
			"",
			"/due ((ABGABEDATUMDDMMYYYY))"
		],
		"assignee": "j.vondoemming",
		"label": "Uebungsblatt"
	},
	"template_links": ["Makefile", "latexindent.yml"]
}
```

## Links

- https://docs.gitlab.studip.de/entwicklung/docs/jsonapi
