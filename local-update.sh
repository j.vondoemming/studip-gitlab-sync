#!/bin/bash

set -e

pushd "$(dirname "$0")" >/dev/null

TYPST_VERSION="0.12.0"
if [ -x "./template/typst/typst" ] || ./template/typst/typst --version | grep -q " ${TYPST_VERSION} " ; then
	echo "Typst already v${TYPST_VERSION}"
else
	wget -O "typst.tar.xz" "https://github.com/typst/typst/releases/download/v${TYPST_VERSION}/typst-x86_64-unknown-linux-musl.tar.xz"
	tar xvf typst.tar.xz
	mv typst-x86_64-unknown-linux-musl/typst template/typst/
	rm -rvf "typst.tar.xz" "typst-x86_64-unknown-linux-musl/"
fi

popd >/dev/null
