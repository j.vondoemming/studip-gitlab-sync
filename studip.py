#!/usr/bin/env python3

import os
import re
import json
from typing import Dict, List, Optional, Set, Tuple, cast
# https://docs.python.org/3/library/urllib.request.html#module-urllib.request
from urllib.parse import urlencode
from urllib.error import HTTPError
import urllib.request
import http.client
from pathlib import Path
import copy
import shutil
import zipfile


STUDIP_BASE_URL = "https://studip.uni-goettingen.de"
_usercache = {}

type HTTPHeaders = Dict[str, str]


def http_get_request(url: str, headers: HTTPHeaders, accept: str = "application/vnd.api+json,application/json") -> bytes:
    """
    Perform a simple HTTP GET request.

    url is the full URL.

    headers are the HTTP headers. Authentication information should be included here.

    accept will be used for the "Accept" HTTP Header.
    """
    headers["Accept"] = accept
    # print("HTTP GET REQUEST: url:", url, "headers:", headers)
    req = urllib.request.Request(
        url=url,
        headers=headers,
        method="GET",
    )
    try:
        res = urllib.request.urlopen(req)
    except HTTPError as e:
        print("http_get_request: HTTP ERROR!", e)
        print("  URL:", e.url)
        print("  Code:", e.code)
        print("  Reason:", e.reason)
        print("  Headers:")
        for k, v in e.headers.items():
            print(f"    {k}: {v}")
        body = e.fp.read()
        try:
            body = "\n    ".join(json.dumps(json.loads(body), indent=2).splitlines())
            print("  Body(json):", body)
        except Exception:
            print("  Body:", body)
        raise e
    if not isinstance(res, http.client.HTTPResponse):
        raise Exception("http_get_request: request somehow didn't result in an HTTPResponse.", url, res)
    if res.getcode() != 200:
        raise Exception("http_get_request: request didn't return 200 OK.", url, res, res.getcode())
    body = res.read()
    if not res.closed:
        res.close()
    return body


def studip_jsonapi(
        route: str,
        headers: HTTPHeaders,
        params: Optional[Dict[str, str | int | float]] = None,
) -> Dict:
    """
    Do a simple authenticated GET request to the Stud.IP JSON:API.

    https://docs.gitlab.studip.de/entwicklung/docs/jsonapi

    route is the route of the API to be accessed. E.g. "v1/courses"

    headers are the HTTP headers. Authentication information should be included here.

    params are the parameters that will be added to the url via urlencoding
    """
    url = STUDIP_BASE_URL + "/jsonapi.php/" + route + ("?" + urlencode(params) if params else "")
    return json.loads(http_get_request(url, headers))


def studip_jsonapi_paginated(
        route: str,
        headers: HTTPHeaders,
        limit: int = 100,
        extraparams: Optional[Dict[str, str | int | float]] = None,
) -> List:
    """
    Do a simple authenticated GET request to the Stud.IP JSON:API on a paginated route.
    The result will be a list containing all items.

    https://docs.gitlab.studip.de/entwicklung/docs/jsonapi

    route is the route of the API to be accessed. E.g. "v1/courses"

    headers are the HTTP headers. Authentication information should be included here.

    limit is the limit of items per page for pagination.

    extraparams are the parameters that will be added to the url via urlencoding
    """
    initialparams: Dict[str, str | int | float] = {
        "page[offset]": 0,
        "page[limit]": limit,
    }
    initialparams.update(extraparams or {})
    resp = studip_jsonapi(headers=headers, route=route, params=initialparams)
    total = resp["meta"]["page"]["total"]
    data: List = resp["data"]
    while "next" in resp["links"]:
        url = STUDIP_BASE_URL + resp["links"]["next"]
        resp = json.loads(http_get_request(url, headers))
        data += resp["data"]
    if len(data) != total:
        raise Exception("Number of items received from the API does not match the initial expected total!", total, len(data), route, limit, data)
    return data


def studip_get_user(user_id: str | Dict, headers: HTTPHeaders) -> Dict:
    if not isinstance(user_id, str):
        assert user_id["data"]["type"] == "users"
        user_id = user_id["data"]["id"]

    if user_id not in _usercache:
        try:
            user = studip_jsonapi(f"v1/users/{user_id}", headers)["data"]
        except Exception as e:
            print("Warning: Failed to get user data.", e)
            return {
                "attributes": {
                    "username": "UNKNOWN",
                    "formatted-name": "UNKNOWN",
                    "email": "UNKNOWN",
                },
                "meta": {
                    "avatar": {
                        "normal": "https://studip.uni-goettingen.de/pictures/user/nobody_normal.png"
                    }
                }
            }
        _usercache[user_id] = user
    return _usercache[user_id]


def studip_get_course_root_folder(course_id: str, headers: HTTPHeaders) -> Dict:
    """
    Get the entire folder structure with information about every folder and file associated with a course.
    """
    # Fetch folder and file information from Stud.IP
    folderslist = studip_jsonapi_paginated(f"v1/courses/{course_id}/folders", headers)
    filerefs = studip_jsonapi_paginated(f"v1/courses/{course_id}/file-refs", headers)

    folders = {}  # folder_id -> folder
    root = None

    # Populate folders from folderslist, prepare folders, and find root
    for folder in folderslist:
        assert folder["type"] == "folders", f"Folder object type is not 'folders': {folder["type"]}"
        assert folder["attributes"]["folder-type"] in ["RootFolder", "MaterialFolder", "HomeworkFolder", "StandardFolder", "CourseGroupFolder", "TimedFolder"], f"Unknown folder type: {folder["attributes"]["folder-type"]}"
        if folder["attributes"]["folder-type"] == "RootFolder":
            assert root is None, "Multiple RootFolder found."
            folder["path"] = "./"
            root = folder
        folder["subfolders"] = {}
        folder["files"] = {}
        folders[folder["id"]] = folder
    assert root is not None, "No RootFolder found."

    # Populate "subfolders" of every folder
    for folder in folderslist:
        if folder is root:
            continue
        assert folder["relationships"]["parent"]["data"]["type"] == "folders", "Parent of folder isn't a folder."
        parent = folders[folder["relationships"]["parent"]["data"]["id"]]
        name = folder["attributes"]["name"]
        assert name not in parent["subfolders"], "Multiple subfolders with same name in folder."
        parent["subfolders"][name] = folder

    # Populate "path" of every folder
    while any(map(lambda folder: "path" not in folder, folderslist)):
        # TODO: Optimize this.
        for folder in folderslist:
            if "path" in folder:
                continue
            parent = folders[folder["relationships"]["parent"]["data"]["id"]]
            if "path" not in parent:
                continue
            name = folder["attributes"]["name"]
            assert "/" not in name
            assert ".." not in name
            folder["path"] = parent["path"] + name + "/"

    # Populate "files" of every folder and set "path" of every file
    for file in filerefs:
        assert file["type"] == "file-refs", f"File object type is not 'file-refs': {file["type"]}"
        assert file["relationships"]["parent"]["data"]["type"] == "folders", "Parent of file isn't a folder."
        parent = folders[file["relationships"]["parent"]["data"]["id"]]
        name = file["attributes"]["name"]
        file["attributes"]["downloads"] = -1  # we don't care about downloads
        file["path"] = parent["path"] + name
        assert name not in parent["files"], "Multiple files with same name in folder."
        parent["files"][name] = file

    # Ensure we didn't miss a file.
    for folder in folderslist:
        assert folder["relationships"]["file-refs"]["meta"]["count"] == len(folder["files"])

    # The RootFolder has references to the original file-refs api response as they are easy to compare.
    root["allfiles"] = filerefs

    return root


def studip_get_course_blubber(course_id: str, headers: HTTPHeaders) -> Tuple[List[Dict], str]:
    threads = studip_jsonapi_paginated(f"v1/courses/{course_id}/blubber-threads", headers)
    if len(threads) == 0:
        return [], ""
    assert len(threads) == 1, f"Veranstaltung hat mehr als einen Blubber Thread: {len(threads)} Threads"
    assert threads[0]["type"] == "blubber-threads"
    thread_id = threads[0]["id"]
    thread_name = threads[0]["attributes"]["name"]
    thread_info_html = threads[0]["attributes"]["context-info"]
    comments = studip_jsonapi_paginated(f"v1/blubber-threads/{thread_id}/comments", headers)
    comments.sort(key=lambda comment: comment["attributes"]["mkdate"])
    if len(comments) == 0:
        return [], ""
    comments_html = ""
    for comment in comments:
        assert comment["type"] == "blubber-comments"
        author = studip_get_user(comment["relationships"]["author"], headers)
        edited = comment["attributes"]["mkdate"] != comment["attributes"]["chdate"]
        comments_html += f"""
<div class="comment" id="{comment["id"]}" data-edited="{edited}">
    <div class="comment__avatar">
        <img src="{author["meta"]["avatar"]["normal"]}" alt="" />
    </div>
    <div class="comment__bubble">
        <a class="comment__author" href="{STUDIP_BASE_URL}/dispatch.php/profile?username={author["attributes"]["username"]}">{author["attributes"]["formatted-name"]} (@{author["attributes"]["username"]})</a>
        <div class="comment__content">
            {comment["attributes"]["content-html"]}
        </div>
        <div class="comment__time">
            <time class="comment__mkdate" datetime="{comment["attributes"]["mkdate"]}">{comment["attributes"]["mkdate"]}</time>
            <time class="comment__chdate" datetime="{comment["attributes"]["chdate"]}">{comment["attributes"]["chdate"]}</time>
        </div>
    </div>
</div>
"""

    html = f"""
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{thread_name} | Blubber</title>
<style>
* {{
  scroll-behavior: smooth;
}}

#top, #bottom {{
  text-align: center;
  border-radius: 0.8rem;
  padding: 0.5rem 1rem;
  width: 100%;
  background-color: #eeeeee;
  margin-left: 10rem;
}}

h1 {{
  text-align: center;
}}

body {{
  padding: 0rem 0.5rem;
}}

main {{
  max-width: 120ch;
  margin-inline: auto;
  gap: 1rem;
  display: grid;
  padding-bottom: 3rem;
}}

.comment {{
  /* outline: 1px solid green; */
  display: flex;
  flex-direction: row;
  gap: 0.3rem;
}}

.comment__avatar img {{
  height: 3rem;
  aspect-ratio: 1/1;
  border-radius: 1.5rem;
  margin-top: calc(100% - 3rem);
}}

.comment__avatar {{
  display: flex;
  align-items: flex-end;
}}

.comment__bubble {{
  border-radius: 0.6rem;
  padding: 0.6rem 1rem;
  background: #e7ebf1;
}}

.comment__content * {{
  max-width: 100%;
}}

.comment__content blockquote {{
  background-color: lightgray;
  margin: 0;
  padding: 0.1rem 1rem;
  border-left: 0.3rem solid gray;
}}

.comment__content blockquote .author {{
  font-weight: bold;
}}

.comment__chdate {{
  display: none;
}}

.comment__time {{
  text-align: end;
  font-size: small;
}}

.comment[data-edited=True] .comment__chdate {{
  display: inline;
  color: red;
}}
</style>
    </head>
    <body>
        <h1>Blubber: {thread_name} [{len(comments)}]</h1>
        <a id="top" href="#bottom">NACH UNTEN</a>
        <main>
            {comments_html}
        </main>
        <a id="bottom" href="#top">NACH OBEN</a>
        <aside class="threadinfo">
        {thread_info_html}
        </aside>
        <!-- The Cake Is A Lie! -->
    </body>
</html>
"""
    return comments, html


def studip_get_course_news(course_id: str, headers: HTTPHeaders) -> Tuple[List[Dict], str]:
    """
    Get all news from a course.

    Returns the list of news objects and a markdown version of the news.

    https://docs.gitlab.studip.de/entwicklung/docs/jsonapi/news
    """
    news = studip_jsonapi_paginated(f"v1/courses/{course_id}/news", headers)
    if not news:
        return [], ""
    news.sort(key=lambda newsitem: newsitem["attributes"]["chdate"], reverse=True)
    newsmd = f"""---
lang: de
autogenerated_warning: Diese Datei wird automatisch generiert. Nicht manuell editieren.
lastchange: {news[0]["attributes"]["chdate"]}
count: {len(news)}
---

Ankündigungen
=============

[TOC]
"""
    for newsitem in news:
        assert newsitem["type"] == "news"
        attributes = newsitem["attributes"]
        comments = newsitem["comments"] = studip_jsonapi(f"v1/news/{newsitem["id"]}/comments", headers)["data"]
        title = attributes["title"] or "(Kein Title)"
        content = attributes["content"] or "(Kein Inhalt)"
        content = content.replace("<!--HTML-->", "")
        content = content.replace("<br>", "<br />").replace("<br >", "<br />").replace("<br/>", "<br />")
        content = content.replace("<br /><br />", "\n\n")
        content = content.replace("<br />", "\\\n")
        content = content.replace("<strong>", "**").replace("</strong>", "**")
        content = content.replace("<p>", "\n").replace("</p>", "\n\n")
        mkdate = attributes["mkdate"]
        chdate = attributes["chdate"]
        author = studip_get_user(newsitem["relationships"]["author"], headers)["attributes"]
        newsmd += f"""
{title}
{"-" * max(len(title), 5)}

Erstellungsdatum: {mkdate} \\
Änderungsdatum: {chdate} \\
Author: [{author["formatted-name"]} (@{author["username"]})]({STUDIP_BASE_URL}/dispatch.php/profile?username={author["username"]})

{content}
"""
        if len(comments):
            newsmd += f"""

Kommentare ({len(comments)})
--------------
```json
{json.dumps(comments, indent=2)}
```
(TODO better comment support)
"""
    return news, newsmd


def studip_download_files(files: List[Dict], headers: HTTPHeaders):
    for file in files:
        path: Path = cast(Path, file["path"])
        # url = STUDIP_BASE_URL + file["meta"]["download-url"]
        url = STUDIP_BASE_URL + "/api.php/file/" + file["id"] + "/download"  # TODO this is using the old API, dowload-url does not support Basic Authorization
        print(f"Downloading '{path}' ({file["attributes"]["filesize"]}b) from '{url}'.")
        path.parent.mkdir(parents=True, exist_ok=True)
        path.write_bytes(http_get_request(url, headers, accept="*/*"))
        assert file["attributes"]["filesize"] == path.stat().st_size, f"Downloaded file has not the expected size. StudIP: {file["attributes"]["filesize"]}b Local: {path.stat().st_size}b"


def determine_files_that_need_updating(root_folder_new: Dict, root_folder_prev: Optional[Dict]) -> List[Dict]:
    res: Set[str] = set()

    print("Determining which files need updating:")

    files_new = {}
    files_prev = {}
    for file in root_folder_new["allfiles"]:
        file = copy.copy(file)
        file["path"] = Path(file["path"])
        files_new[file["id"]] = file
    for file in (root_folder_prev["allfiles"] if root_folder_prev else []):
        file = copy.copy(file)
        file["path"] = Path(file["path"])
        files_prev[file["id"]] = file

    for file_id, file_new in files_new.items():
        path: Path = cast(Path, file_new["path"])
        if file_new["attributes"]["filesize"] == 0:
            print(f"  {path}: zero byte file")
        elif not path.exists():
            print(f"  {path}: Missing.")
            res.add(file_id)
        elif not path.is_file():
            raise Exception("File is not a file.", path)
        elif file_id not in files_prev:
            print(f"  {path}: File is new on StudIP.")
            res.add(file_id)
        elif files_prev[file_id]["attributes"]["chdate"] != file_new["attributes"]["chdate"]:
            print(f"  {path}: File has been updated on StudIP.")
            res.add(file_id)
        elif file_new["attributes"]["filesize"] != path.stat().st_size:
            print(f"  {path}: Mismatch in filesize. StudIP: {file_new["attributes"]["filesize"]}b Local: {path.stat().st_size}b")
            res.add(file_id)
        else:
            pass  # file did not change
    print(f"{len(res)} of {len(files_new)} files are not up-to-date.")
    return [files_new[file_id] for file_id in res]


def process_exercises(input_dir: Path, output_dir: Path, template_dir: Path, exercises: Dict):
    assert input_dir.exists() and input_dir.is_dir(), f"Übungsordner existiert nicht: '{input_dir}'"
    output_dir.mkdir(parents=True, exist_ok=True)
    unknown_files: Set[str] = set(os.listdir(input_dir))
    for exercise_id, exercise in exercises.items():
        input_file: Path = input_dir / exercise["aufgabenblatt"]
        exercise_dir: Path = output_dir / exercise_id
        lock_file: Path = exercise_dir / ".lock"
        if input_file.name in unknown_files:
            unknown_files.remove(input_file.name)
        if not lock_file.exists():
            if not (input_file.exists() and input_file.is_file()):
                continue  # Aufgabenblatt existiert noch nich
            elif exercise_dir.exists() and exercise_dir.is_dir():
                continue  # Exercise already processed
            elif exercise.get("ignore", False):
                continue  # This file is configured to be ignored
        abgabedatum: str | None = exercise.get("abgabe", None)
        if not abgabedatum:
            raise Exception("Übung mit unbekanntem Abgabedatum!", exercise_id, exercise)

        print("")
        print("=" * 40)
        print("NEUES ÜBUNGSBLATT!", exercise_id, exercise)
        print("=" * 40)
        print("")

        exercise_dir.mkdir(parents=True, exist_ok=False)
        lock_file.touch(exist_ok=True)
        shutil.copy(input_file, exercise_dir)
        if input_file.name.endswith(".zip"):
            print("EXTRACTING ZIP:", input_file)
            with zipfile.ZipFile(input_file, 'r') as zip_ref:
                zip_ref.extractall(exercise_dir)

        # link to files in tempalte dir or copy template
        for src_file in template_dir.iterdir():
            dst_file = exercise_dir / Path("./" + str(src_file).removeprefix(str(template_dir)))
            dst_file = Path(str(dst_file).replace("BLATTNUMMER", exercise_id))
            if src_file.is_file():
                text = src_file.read_text(encoding="utf-8", errors='ignore')
                if "((ABGABEDATUM))" in text or "((BLATTNUMMER))" in text:
                    # File is a template
                    text = text.replace("((ABGABEDATUM))", abgabedatum)
                    text = text.replace("((BLATTNUMMER))", exercise_id)
                    dst_file.touch(exist_ok=False)
                    dst_file.write_text(text)
                else:
                    # Create symlink to file
                    os.symlink(Path("../../") / src_file, dst_file)
            elif src_file.is_dir():
                # Create symlink to directory
                os.symlink(Path("../../") / src_file, dst_file, target_is_directory=True)
            else:
                raise Exception("Don't know what src_file is.", src_file)
        lock_file.unlink(missing_ok=False)
    if unknown_files:
        raise Exception(f"Nicht konfigurierete Dateien in '{input_dir}':", unknown_files)


def main():
    os.environ["no_proxy"] = "*"  # Just in case someone uses this code on MacOS and this code uses os.fork somewhere in the future. https://docs.python.org/3/library/urllib.request.html#module-urllib.request

    with open("config.json") as fp:
        config: Dict = json.load(fp)
    course_name = cast(str, config["name"])
    course_link = cast(str, config["link"])
    if "sem_id=" in course_link:
        course_id = course_link.split("sem_id=")[1].split("&")[0]
    elif "cid=" in course_link:
        course_id = course_link.split("cid=")[1].split("&")[0]
    elif len(course_link) == 32 and re.match("^[a-f0-9]*$", course_link):
        course_id = course_link
    else:
        raise Exception("Konnte Veranstaltungs ID nicht aus dem Veranstaltungs Link lesen. Hat der Link gar nichts mit der Veranstaltung zu tun?", course_link)

    print("Veranstaltung:", course_name)
    print("Link:", course_link)
    print("ID:", course_id)

    assert len(course_id) == 32 and re.match("^[a-f0-9]*$", course_id), "Veranstaltungs ID ist nicht valide."

    auth = "Basic " + os.environ["STUDIP_BASIC_AUTH"]
    headers: HTTPHeaders = {
        "Authorization": auth,
    }

    # Get folder and file information
    root_folder = studip_get_course_root_folder(course_id, headers)

    # Download files
    if Path("folders.json").exists():
        with open("folders.json") as fp:
            root_folder_prev = json.load(fp)
    else:
        root_folder_prev = None
    new_files = determine_files_that_need_updating(root_folder, root_folder_prev)
    studip_download_files(new_files, headers)

    # Update folders.json
    with open("folders.json", "w") as fp:
        json.dump(root_folder, fp, indent="\t")

    # Process exercises
    if config["hat_uebungsblaetter"]:
        input_dir = Path(config["aufgabenblatt_ordner"])
        output_dir = Path(config["uebungen_ordner"])
        template_dir = Path(config.get("template_dir", "template/"))
        process_exercises(input_dir, output_dir, template_dir, config["uebungen"])

    # Get blubber messages
    blubber_comments, blubber_html = studip_get_course_blubber(course_id, headers)
    if blubber_comments:
        # with open("blubber.json", "w") as fp:
        #     json.dump(blubber_comments, fp, indent="\t")
        with open("blubber.html", "w") as fp:
            fp.write(blubber_html)

    # Get news information
    news, newsmd = studip_get_course_news(course_id, headers)
    if news:
        # with open("news.json", "w") as fp:
        #     json.dump(news, fp, indent="\t")
        with open("NEWS.md", "w") as fp:
            fp.write(newsmd)


if __name__ == "__main__":
    main()
