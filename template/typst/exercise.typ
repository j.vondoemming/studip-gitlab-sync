#let exercise_generic(
	veranstaltung: "N/A",
	blattnummer: "01",
	abgabe: "??.??.20??, ?? Uhr",
	semester: "Wintersemester ??/??",
	tutor: "N/A",
	gruppe: 1,
	authors: (),
	lang: "de",
	doc,
) = {
	let t = (
		übungsblatt: "Übungsblatt",
		abgabe: "Abgabe",
		übungsgruppe: "Übungsgruppe",
		tutor: "Tutor",
	)
	if lang == "en" {
		t = (
			übungsblatt: "Sheet",
			abgabe: "Deadline",
			übungsgruppe: "Group",
			tutor: "Tutor",
		)
	}
	let subtitle = [#t.übungsblatt #blattnummer | #t.abgabe #abgabe]

	set document(author: authors, title: subtitle + " | " + veranstaltung) // metadata (often shown as title of the window/tab)
	set text(lang: lang)

	set page(paper: "a4")

	set text(font: "New Computer Modern") // the default LaTeX font

	show raw.where(block: true): it => block(
		fill: rgb("#1d2433"),
		inset: 8pt,
		radius: 5pt,
		text(fill: rgb("#a2aabc"), font: "UbuntuMono Nerd Font", size: 7pt, it)
	)
	show raw.where(block: false): it => box(
		fill: rgb("#1d2433"),
		inset: 8pt,
		radius: 5pt,
		text(fill: rgb("#a2aabc"), font: "UbuntuMono Nerd Font", size: 7pt, it)
	)

	let has_tutor = tutor != none and tutor != "N/A"
	let header = {
		set text(size: 10pt)
		grid(
			columns: (1fr, 1fr, 1fr),
			align: (left, center, right),
			authors.join("\n"),
			[
				* #veranstaltung *
			],
			[
				#semester \
				#t.übungsblatt #blattnummer\
				#t.abgabe #abgabe
			],
		)
		if has_tutor {
			grid(
				columns: (1fr, 1fr),
				align: (left, right),
				[#t.übungsgruppe: #gruppe], [#t.tutor: #tutor],
			)
		}
		line(length: 100%)
	}

	set page(
		header-ascent: 10%,
		header: context { if counter(page).get().first() > 1 { header }},
		margin: (top: 8em)
	)

	set heading(numbering: none)
	set math.equation(numbering: "(1)")
	set page(numbering: "1 / 1")

	show "ZZ": { // Zu Zeigen
		let offset = 0.13em
		box(inset: offset, text(size: 1em - 2 * offset,
			box(move("Z", dx: offset, dy: offset)) + box(width: 0pt, move("Z", dx: -0.6em - offset, dy: -offset)))
		)
	}
	show "QED": align(right, $square$) // q.e.d.

	// Title page
	{
		grid(
			columns: (1fr, 1fr),
			align: (left, right),
			authors.join("\n"), semester,
		)
		align(center, [
			= #veranstaltung
			*#subtitle* \
		])
		if has_tutor {
			grid(
				columns: (1fr, 1fr),
				align: (left, right),
				[#t.übungsgruppe: #gruppe], [#t.tutor: #tutor],
			)
		}
		line(length: 100%)
	}

	doc
}
