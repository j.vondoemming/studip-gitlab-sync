#!/bin/bash

# Exit on error
set -e

pushd "$(dirname "${0}")" > /dev/null


# https://docs.gitlab.studip.de/entwicklung/docs/jsonapi
# ~~https://docs.studip.de/develop/Entwickler/RESTAPI~~
# ~~https://studip.uni-goettingen.de/api.php/discovery~~

if ! command -v read &> /dev/null ; then
	printf "\e[31mERROR\e[0m: Befehl 'read' nicht gefunden.\n" >&2
	exit 1
elif ! command -v curl &> /dev/null ; then
	printf "\e[31mERROR\e[0m: Befehl 'curl' nicht gefunden.\n" >&2
	exit 1
elif ! command -v jq &> /dev/null ; then
	printf "\e[31mERROR\e[0m: Befehl 'jq' nicht gefunden.\n" >&2
	exit 1
elif ! command -v realpath &> /dev/null ; then
	printf "\e[31mERROR\e[0m: Befehl 'realpath' nicht gefunden.\n" >&2
	exit 1
elif ! command -v sed &> /dev/null ; then
	printf "\e[31mERROR\e[0m: Befehl 'sed' nicht gefunden.\n" >&2
	exit 1
elif ! command -v grep &> /dev/null ; then
	printf "\e[31mERROR\e[0m: Befehl 'grep' nicht gefunden.\n" >&2
	exit 1
elif ! command -v cut &> /dev/null ; then
	printf "\e[31mERROR\e[0m: Befehl 'cut' nicht gefunden.\n" >&2
	exit 1
elif ! command -v basename &> /dev/null ; then
	printf "\e[31mERROR\e[0m: Befehl 'basename' nicht gefunden.\n" >&2
	exit 1
elif ! command -v sha256sum &> /dev/null ; then
	printf "\e[31mERROR\e[0m: Befehl 'sha256sum' nicht gefunden.\n" >&2
	exit 1
elif ! command -v find &> /dev/null ; then
	printf "\e[31mERROR\e[0m: Befehl 'find' nicht gefunden.\n" >&2
	exit 1
elif ! command -v git &> /dev/null ; then
	printf "\e[31mERROR\e[0m: Befehl 'git' nicht gefunden.\n" >&2
	exit 1
fi
if ! command -v glab &> /dev/null ; then
	printf "\e[33mWARNING\e[0m: Befehl 'glab' nicht gefunden.\n" >&2
	printf "\e[33mWARNING\e[0m: Kann keine GitLab Issues erstellen!\n" >&2
	printf "\e[33mWARNING\e[0m: Bitte installiere und konfiguriere die *offizielle* GitLab CLI: https://gitlab.com/gitlab-org/cli#installation\n" >&2
fi

if [ "x${1}y" != "xnoupdatey" ]; then
	# automatically update this git repo
	git pull

	# automatically update this script
	if [ ! -f ".noselfupdate" ]; then
		curl -o "./update.sh" "https://gitlab.gwdg.de/j.vondoemming/studip-gitlab-sync/-/raw/main/update.sh?ref_type=heads&inline=false"
		./update.sh noupdate
		exit 0
	else
		printf "\e[36mINFO\e[0m: .noselfupdate gefunden. Skript wird nicht automatisch aktualisiert.\n"
	fi
fi

if [ -z "${STUDIP_BASIC_AUTH_USER}" ]; then
	if [ "${USER}" = "jake" ] || [ "${USER}" = "j.vondoemming" ]; then
		STUDIP_BASIC_AUTH_USER="j.vondoemming"
	else
		read -r -p "Stud.IP Nutzername: " -e STUDIP_BASIC_AUTH_USER
	fi
fi
if [ -z "${STUDIP_BASIC_AUTH_PASSWORD}" ]; then
	read -r -p "Stud.IP Passwort für ${STUDIP_BASIC_AUTH_USER}: " -e -s STUDIP_BASIC_AUTH_PASSWORD
	echo ""
fi

STUDIP_BASIC_AUTH=$(echo -ne "${STUDIP_BASIC_AUTH_USER}:${STUDIP_BASIC_AUTH_PASSWORD}" | base64 --wrap 0)
export STUDIP_BASIC_AUTH

if [ ! -f ".noselfupdate" ] || { [ -f ".noselfupdate" ] && [ ! -f "./studip.py" ]; }; then
	curl -o "./studip.py" "https://gitlab.gwdg.de/j.vondoemming/studip-gitlab-sync/-/raw/main/studip.py?ref_type=heads"
fi
python3 ./studip.py
if [ ! -f ".noselfupdate" ]; then
	rm  -f ./studip.py
fi

if [ -x "./local-update.sh" ]; then
	printf "\e[36mINFO\e[0m: local-update.sh gefunden. Führe local-update.sh aus...\n"
	./local-update.sh
	printf "\e[36mINFO\e[0m: local-update.sh ausgeführt.\n"
fi

printf "\n\e[32mINFO\e[0m: ==> Fertig! <==\n\n"

printf "\e[36mINFO\e[0m: Bitte ggf. die folgenden Änderungen Commiten und Pushen!\n"
git status

popd > /dev/null
